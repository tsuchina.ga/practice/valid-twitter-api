package views

import (
    "github.com/gopherjs/vecty"
    "github.com/gopherjs/vecty/event"
    "github.com/gopherjs/gopherjs/js"
    "gitlab.com/tsuchina.ga/practice/valid-twitter-api/gopherjs/components"
)

type HomeView struct {
    vecty.Core
    method string
    endpoint string
    apiKey string
    apiSecret string
    userToken string
    userSecret string
    params map[string]string
    paramKey string
    paramsDom vecty.List
    resultComponent *components.ResultComponent
}

func (v *HomeView) Render() vecty.ComponentOrHTML {
    if v.params == nil {
        v.params = make(map[string]string)
    }

    v.resultComponent = &components.ResultComponent{}

    return vecty.Tag("body",
        vecty.Tag("ons-page",
            vecty.Tag("ons-toolbar",
                vecty.Tag("div", vecty.Markup(vecty.Class("left"))),
                vecty.Tag("div", vecty.Markup(vecty.Class("center")),
                    vecty.Text("valid-twitter-api")),
                vecty.Tag("div", vecty.Markup(vecty.Class("right")),
                    vecty.Tag("ons-toolbar-button", vecty.Markup(event.Click(func(e *vecty.Event) {
                        js.Global.Get("window").Call("open",
                            "https://gitlab.com/tsuchina.ga/practice/valid-twitter-api")
                    })), vecty.Tag("ons-icon", vecty.Markup(
                        vecty.Attribute("icon", "ion-social-github"),
                    )))),
            ),
            vecty.Tag("div",
                vecty.Markup(vecty.Class("content")),
                vecty.Tag("ons-card", vecty.Text(
                    "本システムに入力されたデータはサーバに送信されることなく処理されますので、" +
                        "本システムへの入力データは本システム開発者・管理者はもちろん、" +
                        "第三者に知られることはありません。")),
                vecty.Tag("ons-list",
                    vecty.Tag("ons-list-header", vecty.Text("method")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            vecty.Attribute("placeholder", "ex) GET"),
                            event.Input(func(e *vecty.Event) {
                                v.method = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("endpoint")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.endpoint = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("api key")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.apiKey = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("api secret")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.apiSecret = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("user token")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.userToken = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("user secret")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Style("width", "100%"),
                            vecty.Attribute("type", "text"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.userSecret = e.Target.Get("value").String()
                            })))),
                    vecty.Tag("ons-list-header", vecty.Text("params")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-input", vecty.Markup(
                            vecty.Attribute("value", v.paramKey),
                            vecty.Attribute("placeholder", "param key"),
                            vecty.Attribute("modifier", "underbar"),
                            event.Input(func(e *vecty.Event) {
                                v.paramKey = e.Target.Get("value").String()
                            }))),
                        vecty.Tag("ons-button", vecty.Text("Add params"), vecty.Markup(
                            vecty.Attribute("modifier", "outline"),
                            event.Click(func(e *vecty.Event) {
                                v.createParamsDom()
                            })))),
                    vecty.Tag("ons-list-item", vecty.Tag("ons-list", v.paramsDom)),
                    vecty.Tag("ons-list-header", vecty.Text("generate")),
                    vecty.Tag("ons-list-item",
                        vecty.Tag("ons-button", vecty.Text("Generate"), vecty.Markup(
                            vecty.Attribute("modifier", "outline"),
                            event.Click(func(e *vecty.Event) {
                                v.generate()
                            })))),
                    v.resultComponent))))
}

func (v *HomeView) generate() {
    v.resultComponent.Method = v.method
    v.resultComponent.Endpoint = v.endpoint
    v.resultComponent.ApiKey = v.apiKey
    v.resultComponent.ApiSecret = v.apiSecret
    v.resultComponent.UserToken = v.userToken
    v.resultComponent.UserSecret = v.userSecret
    v.resultComponent.Params = v.params
    v.resultComponent.Generate()
}

func (v *HomeView) createParamsDom() {
    if v.paramKey != "" {
        v.params[v.paramKey] = ""
        v.paramKey = ""

        v.paramsDom = vecty.List{}
        for key, val := range v.params {
            v.paramsDom = append(v.paramsDom, vecty.Tag("ons-list-item",
                vecty.Tag("ons-input", vecty.Markup(
                    vecty.Attribute("value", key),
                    vecty.Attribute("readonly", true),
                )),
                vecty.Tag("ons-input", vecty.Markup(
                    vecty.Attribute("value", val),
                    vecty.Attribute("modifier", "underbar"),
                    vecty.Attribute("name", key),
                    event.Input(func(e *vecty.Event) {
                        key := e.Target.Get("name").String()
                        v.params[key] = e.Target.Get("value").String()
                        //log.Println(v.params)
                    }),
                )),
                //vecty.Tag("ons-button",
                //    vecty.Markup(
                //        vecty.Attribute("modifier", "quiet"),
                //        vecty.Attribute("name", key),
                //        vecty.Attribute("value", val),
                //        event.Click(func(e *vecty.Event) {
                //            key := e.Target.Get("value").String()
                //            log.Println(key)
                //        })),
                //    vecty.Tag("ons-icon",
                //        vecty.Markup(vecty.Attribute("icon", "ion-close-circled")))),
            ))
        }
        vecty.Rerender(v)
    }
}
