package main

import (
    "github.com/go-humble/router"
    "github.com/gopherjs/vecty"
    "gitlab.com/tsuchina.ga/practice/valid-twitter-api/gopherjs/views"
)

func main() {
    // routing
    r := router.New()
    r.ForceHashURL = true

    r.HandleFunc("/", func(context *router.Context) {
        vecty.RenderBody(&views.HomeView{})
    })
    r.Start()
}
