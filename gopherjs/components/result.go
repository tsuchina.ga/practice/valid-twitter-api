package components

import (
    "github.com/gopherjs/vecty"
    "math/rand"
    "strconv"
    "time"
    "net/url"
    "crypto/hmac"
    "crypto/sha1"
    "sort"
    "encoding/base64"
    "strings"
)

type ResultComponent struct {
    vecty.Core
    Method string
    Endpoint string
    ApiKey string
    ApiSecret string
    UserToken string
    UserSecret string
    Params map[string]string
    signatureMethod string
    timestamp string
    nonce string
    version string
    signatureData string
    signatureKey string
    signature string
    authorization string
}

func (c *ResultComponent) Render() vecty.ComponentOrHTML {
    items := vecty.List{}
    items = append(items, vecty.Tag("ons-list-header", vecty.Text("signature method")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.signatureMethod),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("timestamp")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.timestamp),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("nonce")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.nonce),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("version")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.version),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("signature data")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.signatureData),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("signature key")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.signatureKey),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("signature")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.signature),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    items = append(items, vecty.Tag("ons-list-header", vecty.Text("authorization")))
    items = append(items, vecty.Tag("ons-list-item",
       vecty.Tag("ons-input", vecty.Markup(
           vecty.Style("width", "100%"),
           vecty.Attribute("value", c.authorization),
           vecty.Attribute("type", "text"),
           vecty.Attribute("modifier", "underbar"),
           vecty.Attribute("readonly", true)))))

    return vecty.Tag("ons-list", items)
}

func (c *ResultComponent) Generate() {

    c.signatureMethod = "HMAC-SHA1"
    c.timestamp = strconv.FormatInt(time.Now().Unix(), 10)
    c.nonce = randString(32)
    c.version = "1.0"

    params := make(map[string]string)
    params["oauth_consumer_key"] = c.ApiKey
    params["oauth_token"] = c.UserToken
    params["oauth_signature_method"] = c.signatureMethod
    params["oauth_timestamp"] = c.timestamp
    params["oauth_nonce"] = c.nonce
    params["oauth_version"] = c.version
    for key, val := range c.Params {
        params[key] = val
    }

    values := url.Values{}
    for key, val := range params {
        values.Add(key, val)
    }

    signatureData := url.QueryEscape(c.Method)
    signatureData += "&" + url.QueryEscape(c.Endpoint)
    signatureData += "&" + url.QueryEscape(values.Encode())
    c.signatureData = signatureData

    signatureKey := url.QueryEscape(c.ApiSecret)
    signatureKey += "&" + url.QueryEscape(c.UserSecret)
    c.signatureKey = signatureKey

    // signatureの生成
    hash := hmac.New(sha1.New, []byte(signatureKey))
    hash.Write([]byte(signatureData))

    var authKyes []string
    for k := range params {
        authKyes = append(authKyes, k)
    }
    sort.Strings(authKyes)

    c.signature = base64.StdEncoding.EncodeToString(hash.Sum(nil))
    params["oauth_signature"] = c.signature
    authKyes = append(authKyes, "oauth_signature")

    // header authorization生成
    var headVals []string
    for _, k := range authKyes {
        headVals = append(headVals, k + "=" + url.QueryEscape(params[k]))
    }

    c.authorization = "Authorization: OAuth " + strings.Join(headVals, ",")

    vecty.Rerender(c)
}

func randString(n int) string {
    var rs1Letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    b := make([]rune, n)
    for i := range b {
        b[i] = rs1Letters[rand.Intn(len(rs1Letters))]
    }
    return string(b)
}
